# Sir-Appsalot

## Initial Setup

If it's your first time running this, run the following command to set up the SQLite database:

```sh
pnpm db:push
```

## Local Development

Run the following to spin up the web services. The web app will be available on `localhost:8080`:

```sh
pnpm dev
```
