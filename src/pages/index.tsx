import Head from 'next/head';
import Link from 'next/link';
import CardLink from '~/components/CardLink';

export default function Home() {
  return (
    <>
      <Head>
        <title>Desirable Kraken</title>
        <meta name="description" content="Yet another 5e character sheet app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="flex min-h-screen flex-col items-center justify-center bg-gradient-to-b from-[var(--bg-start)] to-[var(--bg)]">
        <div className="container flex flex-col items-center justify-center gap-12 px-4 py-16">
          <h1 className="text-5xl font-extrabold tracking-tight text-white sm:text-[5rem]">
            Track those awesome characters without all the complexity
          </h1>
          <div className="self-start text-lg text-white">
            A &lsquo;roided up character creator based on{' '}
            <Link
              href="https://5e.tools"
              target="_blank"
              className="underline underline-offset-4 transition-all hover:underline-offset-8"
            >
              5e.tools
            </Link>
          </div>
          <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 md:gap-8">
            <CardLink
              href="/characters"
              title="First Steps →"
              description="View a Character Sheet"
            />
            <CardLink
              href="/creator"
              title="Create Character →"
              description="Get into the weeds and make your own character"
            />
          </div>
        </div>
      </main>
    </>
  );
}
