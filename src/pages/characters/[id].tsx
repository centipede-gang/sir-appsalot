import { useRouter } from 'next/router';
import { useState, type ReactElement } from 'react';

import { Stack } from '~/components';
import Abilities from '~/components/CharacterSheet/Abilities';
import Hitpoints from '~/components/CharacterSheet/Hitpoints';
import Inspiration from '~/components/CharacterSheet/Inspiration';
import ProficiencyBonus from '~/components/CharacterSheet/ProficiencyBonus';
import SavingThrows from '~/components/CharacterSheet/SavingThrows';
import Skills from '~/components/CharacterSheet/Skills';
import Speed from '~/components/CharacterSheet/Speed';
import Tabs from '~/components/CharacterSheet/Tabs';
import Header from '~/components/Header';
import { api } from '~/utils/api';
import LayoutWithNavBar from '../../components/layout';
import { type NextPageWithLayout } from '../_app';

const CharacterSheet: NextPageWithLayout = (): ReactElement => {
  const router = useRouter();
  const { data: character } = api.character.getCharacter.useQuery({
    id: router.query.id as string,
  });

  const [inspiration, setInspiration] = useState(true);

  if (!character) return <div>Loading…</div>;

  const { id, speed, skills, classes, hitpoints, abilities } = character;
  const pb =
    Math.ceil(classes.reduce((acc, { level }) => acc + level, 0) / 4) + 1;

  return (
    <Stack
      id="character-sheet"
      direction="column"
      spacing={4}
      className="h-full w-full flex-grow"
    >
      <Header
        character={character}
        onEdit={() => console.debug(`${id}: onEdit`)}
        onRefresh={() => console.debug(`${id}: onRefresh`)}
      />
      <Stack wrap className="gap-4">
        <Abilities abilities={abilities} />
        <Speed speed={speed} />
        <ProficiencyBonus pb={pb} />
        <Inspiration
          hasInspiration={inspiration}
          onUseInspiration={() => setInspiration(false)}
        />
        <Hitpoints {...hitpoints} />
      </Stack>
      <Stack
        spacing={4}
        id="character-sheet-3-panel"
        className="h-full flex-grow"
      >
        <Stack direction="column" className="h-full flex-shrink-0">
          <SavingThrows
            abilities={abilities}
            proficiencies={new Set([])} // TODO: Handle proficiencies
            pb={pb}
          />
        </Stack>
        <Stack className="h-full flex-shrink-0 overflow-hidden">
          <Skills abilities={abilities} skills={skills} pb={pb} />
        </Stack>
        <Stack
          direction="column"
          spacing={4}
          className="h-full flex-grow overflow-hidden"
        >
          <Stack spacing={4} className="text-center">
            <Stack
              id="character-sheet-ac"
              direction="column"
              p={1}
              className="rounded-md border-2"
            >
              <h4>INITIATIVE</h4>
              <span>
                {abilities.DEX.mod > 0 ? '+' : ''}
                {abilities.DEX.mod}
              </span>
            </Stack>
            <Stack
              id="character-sheet-ac"
              direction="column"
              p={1}
              className="rounded-md border-2"
            >
              <h4>Armor Class</h4>
              <span>{10}</span>
            </Stack>
          </Stack>
          <Tabs character={character} />
        </Stack>
      </Stack>
    </Stack>
  );
};

CharacterSheet.getLayout = LayoutWithNavBar;

export default CharacterSheet;
