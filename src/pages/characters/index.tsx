import Link from 'next/link';
import { useCallback, type ReactElement } from 'react';

import { ReloadIcon, TrashIcon } from '@radix-ui/react-icons';
import { useSession } from 'next-auth/react';
import { Box, Stack } from '~/components';
import CardLink from '~/components/CardLink';
import LayoutWithNavBar from '~/components/layout';
import { Button } from '~/components/ui/button';
import { api } from '~/utils/api';
import type { NextPageWithLayout } from '../_app';

const CharactersList: NextPageWithLayout = (): ReactElement => {
  const session = useSession();

  const utils = api.useUtils();
  const { data: characters } = api.character.getCharacters.useQuery();
  const { isPending: deletePending, mutateAsync: deleteCharacter } =
    api.character.remove.useMutation();

  const handleDelete = useCallback(
    async (id: string) => {
      await deleteCharacter(
        { id },
        {
          onSuccess: ([data]) => {
            if (!data) {
              console.error(
                'Received 200 success for deletion without response data!',
              );
              return;
            }
            console.log('Deleted character with ID:', data.id);
            void utils.character.getCharacters.invalidate();
          },
          onError: (err) =>
            console.error('Failed to delete character with error:', err),
        },
      );
    },
    [deleteCharacter, utils.character.getCharacters],
  );

  if (session.status === 'unauthenticated') {
    return (
      <Box className="p-4 text-center text-zinc-300">
        Sign in to access your characters
      </Box>
    );
  }

  if (!characters) return <div>Loading…</div>;

  return (
    <Stack direction="column">
      <ul className="flex flex-col space-y-2">
        {characters.length ? (
          characters.map(({ id, name }) => (
            <li key={id}>
              <Stack direction="row" verticalAlignment="center" spacing={4}>
                <Link
                  key={id}
                  href={`/characters/${id}`}
                  className="m-0 rounded-md border border-slate-400 p-4 text-center"
                >
                  {name}
                </Link>
                <Button
                  variant="destructive"
                  size="icon"
                  title={`Delete ${name}`}
                  onClick={() => handleDelete(id)}
                  {...(deletePending && { disabled: true })}
                >
                  {deletePending ? (
                    <ReloadIcon className="size-4 animate-spin" />
                  ) : (
                    <TrashIcon className="size-4" />
                  )}
                </Button>
              </Stack>
            </li>
          ))
        ) : (
          <CardLink
            href="/creator"
            title="Create a character"
            description="You don't have any characters yet. Go ahead and get started with the character creator!"
          />
        )}
      </ul>
    </Stack>
  );
};

CharactersList.getLayout = LayoutWithNavBar;

export default CharactersList;
