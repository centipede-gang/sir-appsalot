import { GeistSans } from 'geist/font/sans';
import { type Session } from 'next-auth';
import { SessionProvider } from 'next-auth/react';
import { type AppProps } from 'next/app';

import { api } from '~/utils/api';

import { type NextPage } from 'next';
import { type ReactElement, type ReactNode } from 'react';
import '~/styles/globals.css';

export type NextPageWithLayout<P = object> = NextPage<
  PageTransitionEvent,
  NoInfer<P>
> & {
  getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = AppProps<
  PageTransitionEvent & { session: Session | null }
> & {
  Component: NextPageWithLayout;
};

const MyApp = ({
  Component,
  pageProps: { session, ...pageProps },
}: AppPropsWithLayout) => {
  // Use page-level layout, if available
  const getLayout = Component.getLayout ?? ((page) => page);
  return (
    <SessionProvider session={session}>
      <div className={`${GeistSans.className} bg-slate-800 text-white`}>
        {getLayout(<Component {...pageProps} />)}
      </div>
    </SessionProvider>
  );
};

export default api.withTRPC(MyApp);
