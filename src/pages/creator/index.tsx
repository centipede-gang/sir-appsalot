import { zodResolver } from '@hookform/resolvers/zod';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
import { useCallback, type ReactElement } from 'react';
import { useForm } from 'react-hook-form';
import { z } from 'zod';
import { Box } from '~/components';
import { Button } from '~/components/ui/button';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '~/components/ui/form';
import { Input } from '~/components/ui/input';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '~/components/ui/select';
import { Skeleton } from '~/components/ui/skeleton';
import { characterClasses, characterRaces } from '~/server/db/types';
import { api, oneOf } from '~/utils/api';
import LayoutWithNavBar from '../../components/layout';
import { type NextPageWithLayout } from '../_app';

const schema = z.object({
  name: z.string().min(1),
  race: oneOf(characterRaces),
  class: oneOf(characterClasses),
});

const Creator: NextPageWithLayout = (): ReactElement => {
  const router = useRouter();

  const session = useSession();
  const form = useForm<z.infer<typeof schema>>({
    resolver: zodResolver(schema),
    disabled: !session,
    defaultValues: {
      name: '',
      race: 'Human',
      class: 'artificer',
    },
  });

  const { mutateAsync: createCharacter } = api.character.create.useMutation();
  const handleSubmit = useCallback(
    async (vals: z.infer<typeof schema>) => {
      await createCharacter(
        {
          ...vals,
          // Form is not rendered without session being present
          ownerId: session.data!.user.id,
          hitpointsMax: 0,
          classes: [
            {
              class: vals.class,
              level: 42,
            },
          ],
          skills: [],
          notes: '',
        },
        {
          onSuccess: ([newCharacter], _vars) => {
            console.debug('Created', newCharacter);
            if (!newCharacter) {
              console.error('Returned 200 on create but got undefined data!');
              return;
            }
            void router.push(`/characters/${newCharacter.id}`);
          },
        },
      );
    },
    [createCharacter, router, session.data],
  );

  if (session.status === 'unauthenticated') {
    return (
      <Box className="p-4 text-center text-zinc-300">
        Sign in to create a character
      </Box>
    );
  } else if (session.status === 'authenticated' && !session.data.user.id) {
    return <Skeleton className="rounded-md" />;
  }

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(handleSubmit, (err) => console.error(err))}
        className="space-y-4"
      >
        <FormField
          control={form.control}
          name="name"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Character Name</FormLabel>
              <FormControl>
                <Input placeholder="Testy McTesterpants" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="race"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Race</FormLabel>
              <Select onValueChange={field.onChange} defaultValue={field.value}>
                <FormControl>
                  <SelectTrigger>
                    <SelectValue />
                  </SelectTrigger>
                </FormControl>
                <SelectContent>
                  {characterRaces.map((race) => (
                    <SelectItem
                      key={race}
                      value={race}
                    >{`${race[0]!.toUpperCase()}${race.slice(1)}`}</SelectItem>
                  ))}
                </SelectContent>
              </Select>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="class"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Classes</FormLabel>
              <Select onValueChange={field.onChange} defaultValue={field.value}>
                <FormControl>
                  <SelectTrigger>
                    <SelectValue />
                  </SelectTrigger>
                </FormControl>
                <SelectContent>
                  {characterClasses.map((cls) => (
                    <SelectItem
                      key={cls}
                      value={cls}
                    >{`${cls[0]!.toUpperCase()}${cls.slice(1)}`}</SelectItem>
                  ))}
                </SelectContent>
              </Select>
              <FormMessage />
            </FormItem>
          )}
        />
        <Button className="mt-4" type="submit">
          Create Character
        </Button>
      </form>
    </Form>
  );
};

Creator.getLayout = LayoutWithNavBar;

export default Creator;
