export enum DamageType {
  Acid = 'A',
  Bludgeoning = 'B',
  Cold = 'C',
  Fire = 'F',
  Force = 'Fo',
  Lightning = 'L',
  Necrotic = 'N',
  Piercing = 'P',
  Poison = 'Poi',
  Psychic = 'Psy',
  Radiant = 'R',
  Slashing = 'S',
  Thunder = 'T',
}

export type MagicalDamageType =
  | DamageType.Cold
  | DamageType.Fire
  | DamageType.Necrotic
  | DamageType.Radiant
  | DamageType.Acid
  | DamageType.Psychic
  | DamageType.Force;
export type PhysicalDamageType =
  | DamageType.Piercing
  | DamageType.Slashing
  | DamageType.Bludgeoning;

export const abilities = ['DEX', 'STR', 'CON', 'INT', 'WIS', 'CHA'] as const;
export type Ability = (typeof abilities)[number];
export interface AbilityScore {
  score: number;
  mod: number;
}
export type CharacterAbilities = Record<Ability, AbilityScore>;

export const characterSkills = [
  'Acrobatics',
  'Animal Handling',
  'Arcana',
  'Athletics',
  'Deception',
  'History',
  'Insight',
  'Intimidation',
  'Investigation',
  'Medicine',
  'Nature',
  'Perception',
  'Performance',
  'Persuasion',
  'Religion',
  'Sleight of Hand',
  'Stealth',
  'Survival',
] as const;
type Skill = (typeof characterSkills)[number];

export const skillMods: Record<Skill, Ability> = {
  Athletics: 'STR',
  Acrobatics: 'DEX',
  'Sleight of Hand': 'DEX',
  Stealth: 'DEX',
  Arcana: 'INT',
  History: 'INT',
  Investigation: 'INT',
  Nature: 'INT',
  Religion: 'INT',
  'Animal Handling': 'WIS',
  Insight: 'WIS',
  Medicine: 'WIS',
  Perception: 'WIS',
  Survival: 'WIS',
  Deception: 'CHA',
  Intimidation: 'CHA',
  Performance: 'CHA',
  Persuasion: 'CHA',
};

export type { Skill };

export const characterRaces = ['Human', 'Eladrin', 'Half Elf'] as const;
export type CharacterRace = (typeof characterRaces)[number];

export const characterClasses = [
  'artificer',
  'barbarian',
  'bard',
  'cleric',
  'druid',
  'fighter',
  'monk',
  'paladin',
  'ranger',
  'rogue',
  'sorcerer',
  'warlock',
  'wizard',
] as const;
export type CharacterClass = (typeof characterClasses)[number];
