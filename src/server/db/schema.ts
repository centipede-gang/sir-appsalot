import { relations, sql } from 'drizzle-orm';
import {
  index,
  int,
  integer,
  primaryKey,
  sqliteTableCreator,
  text,
} from 'drizzle-orm/sqlite-core';
import { type AdapterAccount } from 'next-auth/adapters';
import {
  type CharacterAbilities,
  type CharacterClass,
  type Skill,
} from './types';

export const createTable = sqliteTableCreator(
  (name) => `desirable_kraken_${name}`,
);

export const users = createTable('user', {
  id: text('id', { length: 255 })
    .notNull()
    .primaryKey()
    .$defaultFn(() => crypto.randomUUID()),
  name: text('name', { length: 255 }),
  email: text('email', { length: 255 }).notNull(),
  emailVerified: int('email_verified', {
    mode: 'timestamp',
  }).default(sql`(unixepoch())`),
  image: text('image', { length: 255 }),
});

export const usersRelations = relations(users, ({ many }) => ({
  accounts: many(accounts),
}));

export const accounts = createTable(
  'account',
  {
    userId: text('user_id', { length: 255 })
      .notNull()
      .references(() => users.id),
    type: text('type', { length: 255 })
      .$type<AdapterAccount['type']>()
      .notNull(),
    provider: text('provider', { length: 255 }).notNull(),
    providerAccountId: text('provider_account_id', { length: 255 }).notNull(),
    refresh_token: text('refresh_token'),
    access_token: text('access_token'),
    expires_at: int('expires_at'),
    token_type: text('token_type', { length: 255 }),
    scope: text('scope', { length: 255 }),
    id_token: text('id_token'),
    session_state: text('session_state', { length: 255 }),
  },
  (account) => ({
    compoundKey: primaryKey({
      columns: [account.provider, account.providerAccountId],
    }),
    userIdIdx: index('account_user_id_idx').on(account.userId),
  }),
);

export const accountsRelations = relations(accounts, ({ one, many }) => ({
  user: one(users, { fields: [accounts.userId], references: [users.id] }),
  characters: many(characters),
}));

export const sessions = createTable(
  'session',
  {
    sessionToken: text('session_token', { length: 255 }).notNull().primaryKey(),
    userId: text('userId', { length: 255 })
      .notNull()
      .references(() => users.id),
    expires: int('expires', { mode: 'timestamp' }).notNull(),
  },
  (session) => ({
    userIdIdx: index('session_userId_idx').on(session.userId),
  }),
);

export const sessionsRelations = relations(sessions, ({ one }) => ({
  user: one(users, { fields: [sessions.userId], references: [users.id] }),
}));

export const verificationTokens = createTable(
  'verification_token',
  {
    identifier: text('identifier', { length: 255 }).notNull(),
    token: text('token', { length: 255 }).notNull(),
    expires: int('expires', { mode: 'timestamp' }).notNull(),
  },
  (vt) => ({
    compoundKey: primaryKey({ columns: [vt.identifier, vt.token] }),
  }),
);

export const characters = createTable('characters', {
  id: text('id', { length: 255 })
    .notNull()
    .primaryKey()
    .$defaultFn(() => crypto.randomUUID()),
  ownerId: text('ownerId').notNull(),
  name: text('name', { length: 255 }).notNull().default(''),
  race: text('race', {
    length: 125,
    enum: ['Human', 'Eladrin', 'Half Elf'],
  })
    .notNull()
    .default('Human'),
  classes: text('class', { mode: 'json' })
    .notNull()
    .$type<{ class: CharacterClass; level: number }[]>(),
  background: text('background', { length: 125 }).default(''),
  hitpointsMax: integer('hitpointsMax', { mode: 'number' })
    .notNull()
    .default(0),
  hitpointsCurrent: integer('hitpointsCurrent', { mode: 'number' })
    .notNull()
    .default(0),
  hitpointsTemp: integer('hitpointsTemp', { mode: 'number' })
    .notNull()
    .default(0),
  speed: integer('speed', { mode: 'number' }).notNull().default(30),
  abilities: text('abilities', { mode: 'json' })
    .$type<CharacterAbilities>()
    .notNull()
    .default({
      DEX: { score: 10, mod: 0 },
      STR: { score: 10, mod: 0 },
      CON: { score: 10, mod: 0 },
      INT: { score: 10, mod: 0 },
      WIS: { score: 10, mod: 0 },
      CHA: { score: 10, mod: 0 },
    }),
  skills: text('skills', { mode: 'json' })
    .$type<Skill[]>()
    .notNull()
    .default([]),
  notes: text('notes', { length: 255 }).notNull().default(''),
});

export const charactersRelations = relations(characters, ({ one }) => ({
  owner: one(accounts, {
    fields: [characters.ownerId],
    references: [accounts.userId],
  }),
}));
