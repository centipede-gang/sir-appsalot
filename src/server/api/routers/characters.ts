import { eq } from 'drizzle-orm';
import { z } from 'zod';
import { characterCreationSchema } from '~/schemas/character';

import { createTRPCRouter, protectedProcedure } from '~/server/api/trpc';
import { db } from '~/server/db';
import { characters } from '~/server/db/schema';

export const iIdSchema = z.object({ id: z.string().uuid() });

export const characterRouter = createTRPCRouter({
  create: protectedProcedure
    .input(characterCreationSchema)
    .mutation(
      async ({
        ctx,
        input: {
          name,
          race,
          speed,
          classes,
          abilities,
          hitpointsMax,
          skills,
          notes,
        },
      }) =>
        ctx.db
          .insert(characters)
          .values({
            ownerId: ctx.session.user.id,
            classes,
            name,
            race,
            speed,
            notes,
            abilities,
            skills,
            hitpointsMax,
            hitpointsCurrent: hitpointsMax,
            hitpointsTemp: 0,
          })
          .returning(),
    ),

  remove: protectedProcedure
    .input(iIdSchema)
    .mutation(async ({ ctx, input: { id } }) =>
      ctx.db.delete(characters).where(eq(characters.id, id)).returning(),
    ),

  getCharacters: protectedProcedure.query(async () =>
    db.query.characters.findMany(),
  ),

  getCharacter: protectedProcedure
    .input(iIdSchema)
    .query(async ({ ctx: { db, session }, input: { id } }) => {
      const ch = await db.query.characters.findFirst({
        where: (chars, { eq }) =>
          eq(chars.ownerId, session.user.id) && eq(chars.id, id),
      });
      const result: Character | null = ch
        ? {
            ...ch,
            hitpoints: {
              current: ch.hitpointsCurrent,
              max: ch.hitpointsMax,
              temp: ch.hitpointsTemp,
            },
          }
        : null;
      return result;
    }),
});

export type Character = Omit<typeof characters.$inferSelect, 'hitpoints'> & {
  hitpoints: {
    current: number;
    max: number;
    temp: number;
  };
};
