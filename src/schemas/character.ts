import { z } from 'zod';
import {
  abilities,
  characterClasses,
  characterRaces,
  characterSkills,
} from '~/server/db/types';
import { oneOf } from '~/utils/api';

const AbilityEnum = z.enum(abilities);

export const characterCreationSchema = z.object({
  ownerId: z.string(),
  name: z
    .string({
      required_error: 'You must specify a character name',
    })
    .min(1),
  race: oneOf(characterRaces),
  classes: z
    .array(
      z.object({
        class: oneOf(characterClasses),
        level: z.number().nonnegative(),
      }),
    )
    .min(1),
  hitpointsMax: z.number().nonnegative(),
  abilities: z
    .record(
      AbilityEnum,
      z.object({
        score: z.number().nonnegative(),
        mod: z.number().nonnegative(),
      }),
    )
    .refine((obj): obj is Required<typeof obj> =>
      AbilityEnum.options.every((k) => obj[k] !== null),
    )
    .default({
      DEX: { score: 10, mod: 0 },
      STR: { score: 10, mod: 0 },
      CON: { score: 10, mod: 0 },
      INT: { score: 10, mod: 0 },
      WIS: { score: 10, mod: 0 },
      CHA: { score: 10, mod: 0 },
    }),
  skills: z.array(oneOf(characterSkills)),
  speed: z.number().nonnegative().default(30),
  notes: z.string(),
});
