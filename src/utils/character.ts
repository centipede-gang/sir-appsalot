export const getCharacterSlug = (id: string, name: string): string =>
  name
    .split(' ')
    .map((s) => s.toLowerCase())
    .join('-');
