import { type Ability } from '~/server/db/types';

export type Entries<T> = Array<
  {
    [K in keyof T]: [K, T[K]];
  }[keyof T]
>;

export function asEntries<T, Obj extends Record<string, T>>(
  obj: Obj | undefined | null,
): Entries<Obj> {
  return Object.entries(obj ?? {}) as Entries<Obj>;
}

export function abilityToDisplayString(ability: Ability): string {
  switch (ability) {
    case 'CHA':
      return 'Charisma';
    case 'CON':
      return 'Constitution';
    case 'DEX':
      return 'Dexterity';
    case 'INT':
      return 'Intelligence';
    case 'STR':
      return 'Strength';
    case 'WIS':
      return 'Wisdom';
  }
}
