/**
 * This is the client-side entrypoint for your tRPC API. It is used to create the `api` object which
 * contains the Next.js App-wrapper, as well as your type-safe React Query hooks.
 *
 * We also create a few inference helpers for input and output types.
 */
import { httpBatchLink } from '@trpc/client';
import { createTRPCNext } from '@trpc/next';
import { ssrPrepass } from '@trpc/next/ssrPrepass';
import { type inferRouterInputs, type inferRouterOutputs } from '@trpc/server';
import superjson from 'superjson';
import { z } from 'zod';

import { type AppRouter } from '~/server/api/root';

const getBaseUrl = () => {
  if (typeof window !== 'undefined') return ''; // browser should use relative url
  if (process.env.DEPLOYED_URL) return `https://${process.env.DEPLOYED_URL}`; // SSR should use deployment url
  return `http://localhost:${process.env.PORT ?? 3000}`; // dev SSR should use localhost
};

/** A set of type-safe react-query hooks for your tRPC API. */
export const api = createTRPCNext<AppRouter>({
  ssr: true,
  ssrPrepass,
  config(opts) {
    const { ctx } = opts;
    if (typeof window !== 'undefined') {
      // during client requests
      return {
        links: [httpBatchLink({ url: '/api/trpc', transformer: superjson })],
      };
    }
    return {
      links: [
        httpBatchLink({
          // The server needs to know your app's full url
          url: `${getBaseUrl()}/api/trpc`,
          /**
           * Set custom request headers on every request from tRPC
           * @see https://trpc.io/docs/v10/header
           */
          // To use SSR properly, you need to forward client headers to the server
          // This is so you can pass through things like cookies when we're server-side rendering
          headers: () => ({
            ...(ctx?.req?.headers?.cookie && {
              cookie: ctx.req.headers.cookie,
            }),
          }),
          transformer: superjson,
        }),
      ],
    };
  },
  transformer: superjson,
});

/**
 * Inference helper for inputs.
 *
 * @example type HelloInput = RouterInputs['example']['hello']
 */
export type RouterInputs = inferRouterInputs<AppRouter>;

/**
 * Inference helper for outputs.
 *
 * @example type HelloOutput = RouterOutputs['example']['hello']
 */
export type RouterOutputs = inferRouterOutputs<AppRouter>;

export function oneOf<
  T extends string | number | boolean | bigint | null | undefined,
>(t: readonly [T, T, ...T[]]) {
  // A union must have at least 2 elements so to work with the types it
  // must be instantiated in this way.
  return z.union([
    z.literal(t[0]),
    z.literal(t[1]),
    // No pointfree here because `z.literal` takes an optional second parameter
    ...t.slice(2).map((v) => z.literal(v)),
  ]);
}
