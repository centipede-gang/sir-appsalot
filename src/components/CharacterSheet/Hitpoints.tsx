import { type ReactElement } from 'react';

interface HitpointsProps {
  max: number;
  current: number;
  temp: number;
}

function Hitpoints({ max, current, temp }: HitpointsProps): ReactElement {
  return (
    <div
      id="character-sheet-hitpoints"
      className="flex flex-col space-y-2 rounded-md border-2 p-2"
    >
      <div id="character-hitpoints" className="flex space-x-4">
        <div className="flex space-x-2">
          <div>
            <h5>CURRENT</h5>
            <div id="character-hitpoints-current">{current}</div>
          </div>
          <span>/</span>
          <div>
            <h5>MAX</h5>
            <div id="character-hitpoints-max">{max}</div>
          </div>
        </div>
        <div>
          <h5>TEMP</h5>
          <div id="character-hitpoints-temp">{temp > 0 ? temp : '—'}</div>
        </div>
      </div>
      <h4>HIT POINTS</h4>
    </div>
  );
}

export default Hitpoints;
