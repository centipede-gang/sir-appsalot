import {
  type ReactElement,
  type TextareaHTMLAttributes,
  useCallback,
  useMemo,
  useState,
} from 'react';
import { type Character } from '~/server/api/routers/characters';
import { debounce } from '~/utils/debounce';
import Stack from '../Stack';

interface NotesTabProps {
  character: Character;
}

export default function NotesTab({ character }: NotesTabProps): ReactElement {
  const updateNotes = useCallback(
    ({ id, notes }: { id: string; notes: string }) =>
      console.debug(`updating notes for ${id}: ${notes}`),
    [],
  );

  const debouncedUpdate = useMemo(
    () => debounce(350, updateNotes),
    [updateNotes],
  );
  const [notes, setNotes] = useState(character.notes);
  const handleUpdate = useCallback<
    Required<TextareaHTMLAttributes<HTMLTextAreaElement>>['onChange']
  >(
    ({ target }) => {
      setNotes(target.value);
      debouncedUpdate({ id: character.id, notes: target.value });
    },
    [character.id, debouncedUpdate],
  );

  return (
    <Stack
      direction="column"
      spacing={2}
      style={{ width: '100%', height: '100%' }}
    >
      <h2>Notes</h2>
      <textarea
        value={notes}
        onChange={handleUpdate}
        className="flex-1 resize rounded-md border border-slate-400 bg-inherit p-2 py-1"
      />
    </Stack>
  );
}
