import { type PropsWithChildren, type ReactElement } from 'react';

interface StatBlockProps extends PropsWithChildren {
  id: string;
  title: string;
}

function StatBlock({ id, title, children }: StatBlockProps): ReactElement {
  return (
    <div
      id={id}
      className="flex w-24 flex-col items-center rounded-md border-2 pt-2"
    >
      <h4
        className="font-bold uppercase text-gray-300"
        style={{ fontSize: '.675rem' }}
      >
        {title}
      </h4>
      {children}
    </div>
  );
}

export default StatBlock;
