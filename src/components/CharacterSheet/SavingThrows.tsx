import { type ReactElement } from 'react';
import {
  type Ability,
  type AbilityScore,
  type CharacterAbilities,
} from '~/server/db/types';

interface SaveBlockProps {
  ability: Ability;
  score: AbilityScore;
  pb: number;
  proficient: boolean;
}

function SaveBlock({
  ability,
  score: { mod },
  pb,
  proficient,
}: SaveBlockProps): ReactElement {
  const finalMod = mod + (proficient ? pb : 0);

  return (
    <div
      id={`saving-throw-block-${ability}`}
      className="flex justify-between rounded-md border-2 p-1"
    >
      <h4 id={`saving-throw-name-${ability}`}>{ability}</h4>
      <p id={`saving-throw-mod-${ability}`} className="text-right">
        {finalMod > 0 ? '+' : ''}
        {finalMod}
      </p>
    </div>
  );
}

interface SavingThrowsProps {
  abilities: CharacterAbilities;
  pb: number;
  proficiencies: Set<Ability>;
}

function SavingThrows({
  abilities,
  pb,
  proficiencies,
}: SavingThrowsProps): ReactElement {
  return (
    <div
      id="character-sheet-abilities"
      className="grid flex-grow-0 grid-cols-2 grid-rows-3 gap-2"
    >
      <SaveBlock
        ability="DEX"
        score={abilities.DEX}
        pb={pb}
        proficient={proficiencies.has('DEX')}
      />
      <SaveBlock
        ability="STR"
        score={abilities.STR}
        pb={pb}
        proficient={proficiencies.has('STR')}
      />
      <SaveBlock
        ability="CON"
        score={abilities.CON}
        pb={pb}
        proficient={proficiencies.has('CON')}
      />
      <SaveBlock
        ability="INT"
        score={abilities.INT}
        pb={pb}
        proficient={proficiencies.has('INT')}
      />
      <SaveBlock
        ability="WIS"
        score={abilities.WIS}
        pb={pb}
        proficient={proficiencies.has('WIS')}
      />
      <SaveBlock
        ability="CHA"
        score={abilities.CHA}
        pb={pb}
        proficient={proficiencies.has('CHA')}
      />
    </div>
  );
}

export default SavingThrows;
