import { type ReactElement } from 'react';

import {
  type Ability,
  type AbilityScore,
  type CharacterAbilities,
} from '~/server/db/types';
import { abilityToDisplayString, asEntries } from '~/utils/helpers';
import StatBlock from './StatBlock';

interface AbilityBlockProps {
  ability: Ability;
  score: AbilityScore;
}

function AbilityBlock({
  ability,
  score: { score, mod },
}: AbilityBlockProps): ReactElement {
  return (
    <StatBlock
      id={`ability-block-${ability}`}
      title={abilityToDisplayString(ability)}
    >
      <p id={`ability-mod-${ability}`}>
        {mod > 0 ? '+' : ''}
        {mod}
      </p>
      <p id={`ability-score-${ability}`}>{score}</p>
    </StatBlock>
  );
}

interface AbilitiesProps {
  abilities: CharacterAbilities;
}

function Abilities({ abilities }: AbilitiesProps): ReactElement {
  return (
    <div id="character-sheet-abilities" className="flex space-x-4 text-center">
      {asEntries(abilities).map(([ability, score]) => (
        <AbilityBlock key={ability} ability={ability} score={score} />
      ))}
    </div>
  );
}

export default Abilities;
