import { type ReactElement } from 'react';

interface ProficiencyBonusProps {
  pb: number;
}

function ProficiencyBonus({ pb }: ProficiencyBonusProps): ReactElement {
  return (
    <div className="flex w-auto flex-col items-center justify-center rounded-md border-2 px-2">
      <h4 className="uppercase">Proficiency</h4>
      <p id="proficiency-bonus" className="flex items-center" role="note">
        <span className="font-bold text-slate-300">+</span>
        <span className="text-2xl font-bold">{pb <= 2 ? 2 : pb}</span>
      </p>
      <h4 className="uppercase">Bonus</h4>
    </div>
  );
}

export default ProficiencyBonus;
