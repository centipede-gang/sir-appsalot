import { type ReactElement, useState, useTransition } from 'react';
import { type Character } from '~/server/api/routers/characters';
import NotesTab from './NotesTab';

const tabs = [
  'Actions',
  'Spells',
  'Inventory',
  'Features & Traits',
  'Description',
  'Notes',
  'Extras',
] as const;
type SheetTabs = (typeof tabs)[number];

const tabClasses =
  'bg-slate-500 px-2 py-1 rounded-t-md border-b-2 cursor-pointer transition-all select-none flex-grow min-h-8';

interface TabsProps {
  character: Character;
}

function Tabs({ character }: TabsProps): ReactElement {
  const [activeTab, setActiveTab] = useState<SheetTabs>('Inventory');
  const [pending, start] = useTransition();
  const updateTab = (tab: SheetTabs) => start(() => setActiveTab(tab));

  return (
    <div
      id="character-sheet-tabs"
      className="flex h-full w-full flex-col space-y-2 rounded-md border-2 p-2"
    >
      <ul className="flex flex-shrink-0 space-x-1 text-center">
        {tabs.map((tab) => (
          <li
            key={tab}
            className={`${tabClasses} ${activeTab === tab ? '' : 'border-b-slate-500'}`}
            onClick={() => updateTab(tab)}
          >
            {tab}
          </li>
        ))}
      </ul>
      <div
        id="character-sheet-tab-content"
        className="flex w-full flex-grow"
        {...(pending && {
          transition: 'color .3s',
          'transition-delay': '.1s',
          'transition-timing-function': 'ease-in',
          color: '#aaa',
        })}
      >
        {activeTab === 'Actions' ? <div>Actions</div> : null}
        {activeTab === 'Spells' ? <div>Spells</div> : null}
        {activeTab === 'Inventory' ? <div>Inventory</div> : null}
        {activeTab === 'Features & Traits' ? (
          <div>Features & Traits</div>
        ) : null}
        {activeTab === 'Description' ? <div>Description</div> : null}
        {activeTab === 'Notes' ? <NotesTab character={character} /> : null}
        {activeTab === 'Extras' ? <div>Extras</div> : null}
      </div>
    </div>
  );
}

export default Tabs;
