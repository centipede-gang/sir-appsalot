import { type ReactElement } from 'react';

import StatBlock from './StatBlock';

interface SpeedProps {
  speed: number;
}

function Speed({ speed }: SpeedProps): ReactElement {
  return (
    <StatBlock id="speed-block" title="Speed">
      <p id="character-speed" className="justify-self-center">
        {speed}
      </p>
    </StatBlock>
  );
}

export default Speed;
