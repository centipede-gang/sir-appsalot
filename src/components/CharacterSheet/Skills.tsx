import { type ReactElement } from 'react';
import {
  type CharacterAbilities,
  type Skill,
  characterSkills,
  skillMods,
} from '~/server/db/types';

interface SkillBlockProps {
  skill: Skill;
  bonus: number;
  proficient: boolean;
}

function SkillBlock({
  skill,
  bonus,
  proficient,
}: SkillBlockProps): ReactElement {
  return (
    <div
      id={`skill-block-${skill}`}
      className="grid grid-cols-8 rounded-md border-2 p-1"
      title={
        proficient ? `Proficient in ${skill}` : `Not Proficient in ${skill}`
      }
    >
      <div
        className={`m-1 h-4 w-4 rounded-lg border-2 ${proficient ? 'bg-teal-400' : 'bg-inherit'}`}
      />
      <h4 id={`skill-name-${skill}`} className="col-span-6 text-left">
        {skill}
      </h4>
      <p id={`skill-bonus-${skill}`} className="text-right">
        {bonus > 0 ? '+' : ''}
        {bonus}
      </p>
    </div>
  );
}

interface SkillsProps {
  abilities: CharacterAbilities;
  skills: Skill[];
  pb: number;
}

function Skills({ abilities, skills, pb }: SkillsProps): ReactElement {
  return (
    <div
      id="character-sheet-skills"
      className="flex min-h-0 flex-col space-y-2 overflow-auto rounded-md border-2 p-2"
    >
      {characterSkills.map((skill) => {
        const { mod } = abilities[skillMods[skill]];
        const proficient = skills.includes(skill);
        const bonus = mod + (proficient ? pb : 0);
        return (
          <SkillBlock
            key={skill}
            skill={skill}
            bonus={bonus}
            proficient={proficient}
          />
        );
      })}
      <h4 className="text-center font-bold text-slate-300">Skills</h4>
    </div>
  );
}

export default Skills;
