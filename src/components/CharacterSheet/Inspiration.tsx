import { type ReactElement } from 'react';

interface InspirationProps {
  hasInspiration: boolean;
  onUseInspiration: () => void;
}

function Inspiration({
  hasInspiration,
  onUseInspiration,
}: InspirationProps): ReactElement {
  return (
    <div
      id="character-sheet-inspiration"
      className="flex flex-col justify-around rounded-md border-2 p-2 text-center align-middle"
      title={
        hasInspiration ? 'You have inspiration' : 'You do not have inspiration'
      }
    >
      <span>
        <input
          type="checkbox"
          disabled={!hasInspiration}
          checked={hasInspiration}
          onChange={() => (hasInspiration ? onUseInspiration() : undefined)}
          className="h-6 w-6 cursor-pointer"
        />
      </span>
      <h4>INSPIRATION</h4>
    </div>
  );
}

export default Inspiration;
