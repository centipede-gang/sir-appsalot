import { signIn, signOut, useSession } from 'next-auth/react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { type ReactElement } from 'react';
import Stack from './Stack';

const activeTab =
  'bg-slate-700 text-white hover:text-white rounded-md px-3 py-2 text-sm font-medium';
const inactiveTab =
  'text-gray-300 hover:bg-gray-700 hover:text-white rounded-md px-3 py-2 text-sm font-medium';

const navbarPaths = [
  ['Home', '/'],
  ['Creator', '/creator'],
  ['Characters', '/characters'],
] as const;

function Auth(): ReactElement {
  const { data: sessionData } = useSession();

  return (
    <Stack direction="row" className="ml-4">
      <p className="mr-2 select-none self-center font-mono text-white">
        {sessionData && <span>{sessionData.user?.name}</span>}
      </p>
      <button
        className={activeTab}
        onClick={sessionData ? () => void signOut() : () => void signIn()}
      >
        {sessionData ? 'Sign out' : 'Sign in'}
      </button>
    </Stack>
  );
}

export default function NavBar(): ReactElement {
  const pathname = usePathname();

  return (
    <nav
      id="app-navbar"
      className="sticky top-0 z-50 flex w-full items-center justify-between bg-slate-600 px-4"
    >
      <div className="relative flex h-16 flex-grow items-center space-x-4">
        {navbarPaths.map(([name, path]) => (
          <Link
            key={path}
            href={path}
            className={pathname === path ? activeTab : inactiveTab}
          >
            {name}
          </Link>
        ))}
      </div>
      <Stack
        id="5etools-search"
        verticalAlignment="center"
        horizontalAlignment="center"
      >
        <input
          type="text"
          className="rounded-md p-1"
          placeholder="Search 5e.tools"
          onChange={({ target: { value } }) => {
            console.log('Searching for:', value);
          }}
        />
      </Stack>
      <Auth />
    </nav>
  );
}
