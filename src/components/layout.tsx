import { type ReactElement } from 'react';
import { Box, NavBar } from '~/components';

export default function LayoutWithNavBar(page: ReactElement): ReactElement {
  return (
    <>
      <NavBar />
      <Box className="mx-16 my-4">{page}</Box>
    </>
  );
}
