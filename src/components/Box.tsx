import type {
  DetailedHTMLProps,
  HTMLAttributes,
  PropsWithChildren,
  ReactElement,
} from 'react';

export interface BoxProps
  extends PropsWithChildren,
    DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  p?: number | 'auto';
  m?: number | 'auto';
}

export default function Box({
  p,
  m,
  className,
  ...divProps
}: BoxProps): ReactElement {
  const classes = [];
  if (p !== undefined) classes.push(`p-${p}`);
  if (m !== undefined) classes.push(`m-${m}`);
  if (className !== undefined) classes.push(className);

  return <div {...divProps} className={classes.join(' ')} />;
}
