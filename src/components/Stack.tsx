import { type ReactElement } from 'react';
import Box, { type BoxProps } from './Box';

type Alignment = 'start' | 'center' | 'end' | 'stretch';

export interface StackProps extends BoxProps {
  direction?: 'row' | 'column';
  spacing?: number | 'between' | 'around';
  verticalAlignment?: Alignment;
  horizontalAlignment?: Alignment;
  wrap?: boolean;
}

export default function Stack({
  direction = 'row',
  spacing = 0,
  verticalAlignment,
  horizontalAlignment,
  wrap = false,
  className,
  ...BoxProps
}: StackProps): ReactElement {
  const classes = ['flex'];
  if (typeof spacing === 'number' && spacing > 0) {
    classes.push(
      direction === 'row' ? `space-x-${spacing}` : `space-y-${spacing}`,
    );
  } else if (spacing === 'around') {
    classes.push('justify-around');
  } else if (spacing === 'between') {
    classes.push('justify-between');
  }

  classes.push(wrap ? 'flex-wrap' : 'flex-nowrap');

  if (direction === 'row') {
    classes.push(
      `align-${verticalAlignment ?? 'normal'}`,
      `justify-${horizontalAlignment ?? 'normal'}`,
    );
  } else {
    classes.push('flex-col');
    classes.push(
      `align-${horizontalAlignment ?? 'normal'}`,
      `justify-${verticalAlignment ?? 'normal'}`,
    );
  }

  if (className) classes.push(className);

  return <Box className={classes.join(' ')} {...BoxProps} />;
}
