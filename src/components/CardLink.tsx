import Link from 'next/link';
import { type ReactElement } from 'react';

interface CardLinkProps {
  href: string;
  title: string;
  description: string;
}

export default function CardLink({
  href,
  title,
  description,
}: CardLinkProps): ReactElement {
  return (
    <Link
      className="flex max-w-xs flex-col gap-4 rounded-xl bg-white/10 p-4 text-white hover:bg-white/20"
      href={href}
    >
      <h3 className="text-2xl font-bold">{title}</h3>
      <div className="text-lg">{description}</div>
    </Link>
  );
}
