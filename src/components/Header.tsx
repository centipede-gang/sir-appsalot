import Image from 'next/image';
import { type ReactElement, useMemo } from 'react';
import type { Character } from '~/server/api/routers/characters';
import Button from './Button';

interface HeaderProps {
  character: Character;
  onEdit?: VoidFunction;
  onRefresh?: VoidFunction;
}

function Header({
  character: { id, name, race, classes },
  onEdit,
  onRefresh,
}: HeaderProps): ReactElement {
  const characterLevel = useMemo(
    () => classes.reduce((acc, { level }) => acc + level, 0),
    [classes],
  );
  const handleEdit = (): void => {
    console.log(`Editing '${name}' (${id})...`);
    onEdit?.();
  };
  const handleRefresh = (): void => {
    console.log(`Refreshing '${name}' (${id})...`);
    onRefresh?.();
  };

  return (
    <div id="character-header" className="flex items-center justify-between">
      <div id="character-profile" className="flex items-center space-x-2">
        <div id="character-avatar">
          <Image src="/avatar.jpg" alt="Player avatar" width={64} height={64} />
        </div>
        <div id="character-info" className="flex flex-col">
          <p className="text-xl font-bold text-white">{name}</p>
          <div id="character-class-info" className="flex space-x-4">
            <p className="text-gray-400">{race}</p>
            <div className="flex space-x-1 text-gray-400">
              {classes.map(({ class: name, level }, idx) => (
                <span key={name}>
                  <span>
                    {name} {level}
                  </span>
                  {idx < classes.length - 1 ? <p>/</p> : null}
                </span>
              ))}
            </div>
          </div>
          <div id="character-level" className="text-gray-200">
            Level {characterLevel}
          </div>
        </div>
      </div>
      <div id="character-buttons" className="flex space-x-2">
        <Button
          title={`Edit ${name}'s character sheet`}
          onClick={handleEdit}
          // startIcon={<Icon icon="ion:edit" />}
        >
          Edit
        </Button>
        <Button
          title="Refresh character sheet"
          onClick={handleRefresh}
          // startIcon={<Icon icon="ion:reload" />}
        >
          Refresh
        </Button>
      </div>
    </div>
  );
}

export default Header;
