import {
  type HTMLAttributes,
  type PropsWithChildren,
  type ReactElement,
  type ReactNode,
} from 'react';

export interface ButtonProps extends PropsWithChildren {
  onClick?: VoidFunction;
  disabled?: boolean;
  variant?: 'filled' | 'outlined' | 'text';
  direction?: 'vertical' | 'horizontal';
  title?: string;
  startIcon?: ReactNode;
  className?: string;
}

export default function Button({
  onClick,
  disabled,
  variant = 'filled',
  direction = 'horizontal',
  children,
  startIcon,
  className,
  ...props
}: ButtonProps): ReactElement {
  const baseClasses: HTMLAttributes<HTMLButtonElement>['className'] =
    'flex items-center justify-center select-none rounded-sm px-6 py-3 text-center align-middle ' +
    'font-mono text-xs font-bold uppercase text-white ' +
    'shadow-md shadow-slate-900/10 transition-all hover:shadow-lg hover:shadow-slate-900/20 hover:bg-slate-700 ' +
    'focus:opacity-[0.85] focus:shadow-none ' +
    'active:opacity-[0.85] active:shadow-none ' +
    'disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none';
  let classes = `${baseClasses} ${className}`;
  if (variant === 'filled') classes += ' bg-slate-900';
  else if (variant === 'outlined')
    classes += ' rounded-md border border-slate-600 text-slate-200';

  if (direction === 'vertical') classes += ' flex-col space-y-2';
  else classes += ' space-x-2';

  return (
    <button
      {...props}
      onClick={onClick}
      disabled={disabled}
      className={classes}
      type="button"
    >
      {startIcon ? (
        <span className="flex items-center">{startIcon}</span>
      ) : null}
      <span className="leading-0">{children}</span>
    </button>
  );
}
